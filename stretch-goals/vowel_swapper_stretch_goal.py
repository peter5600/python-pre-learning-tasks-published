from enum import Enum
class Vowels(Enum):
    A = 0
    E = 1
    I = 2
    O = 3
    U = 4


def vowel_swapper(string):
    # ==============
    # Your code here
    AlreadySeen = [0,0,0,0,0,0]
    for i in range(len(string)):
        if(string[i] == "a" or string[i] == "A"):
            if(AlreadySeen[Vowels.A.value] == 1):
                string = string[:i] + "4" + string[i + 1:]
            AlreadySeen[Vowels.A.value] += 1
            
        elif(string[i] == "e" or string[i] == "E"):
            if(AlreadySeen[Vowels.E.value] == 1):
                string = string[:i] + "3" + string[i + 1:]
            AlreadySeen[Vowels.E.value] += 1
        elif(string[i] == "i" or string[i] == "I"):
            if(AlreadySeen[Vowels.I.value] == 1):
                string = string[:i] + "!" + string[i + 1:]
            AlreadySeen[Vowels.I.value] += 1
        elif(string[i] == "o"):
            if(AlreadySeen[Vowels.O.value] == 1):
                string = string[:i] + "ooo" + string[i + 1:]
            AlreadySeen[Vowels.O.value] += 1
        elif(string[i] == "O"):
            if(AlreadySeen[Vowels.O.value] == 1):
                string = string[:i] + "000" + string[i + 1:]
            AlreadySeen[Vowels.O.value] += 1
        elif(string[i] == "u" or string[i] == "U"):
            if(AlreadySeen[Vowels.U.value] == 1):
                string = string[:i] + "|_|" + string[i + 1:]
            AlreadySeen[Vowels.U.value] += 1
    return string
    # ==============

#The expected output below dosen't match the expect output described on gitlab
#on gitlab it says expected output is Should print "a4a e3e i!i o000o u|_|u" to the console
#I have gone by the requirments on gitlab for this project
print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
