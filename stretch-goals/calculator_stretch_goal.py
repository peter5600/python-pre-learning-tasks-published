# ==============
# Your code here
def ReturnMaxBitValue(Num):
    #To work out binary u need the max bit first
    MaxBit =1
    while MaxBit < Num:
        MaxBit *= 2
    return MaxBit

def Base10ToBase2(Num):
    MaxBit = ReturnMaxBitValue(Num)
    BinaryValue = ""
    while(MaxBit > 0):
        if Num - MaxBit >= 0:
            BinaryValue += "1"
            Num -= MaxBit
        else:
            BinaryValue += "0"
        
        MaxBit = MaxBit //2
    if(BinaryValue[0] == "0"):#the examples ask the first 0 to be removed
        return BinaryValue[1:]
    return BinaryValue

def calculator(a, b, operator):
    
    if operator == "+": 
        return Base10ToBase2(a + b)
    elif operator == "/":
        return Base10ToBase2(a // b)#floor
    elif operator == "-":
        return Base10ToBase2(a - b)
    elif operator == "*":
        return Base10ToBase2(a * b)
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
