def factors(number):
    # ==============
    # Your code here
    results = list()
    for i in range(2,number):
        if(number % i == 0):
            results.append(i)
    if len(results) != 0:
        return results
    elif number == 1:
        return "1 is not a prime but has no factors that aren't 1"#dosen't ask for this but technicaly 1 isnt a prime number and the app says dont return 1
    else:
        return (str(number) + " is a prime number")
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
