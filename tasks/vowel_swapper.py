def vowel_swapper(string):
    # ==============
    # Your code here
    NewString =""
    for i in range(len(string)):
        if(string[i] == "a" or string[i] == "A"):
            NewString += "4"
        elif(string[i] == "e" or string[i] == "E"):
            NewString += "3"
        elif(string[i] == "i" or string[i] == "I"):
            NewString += "!"
        elif(string[i] == "o"):
            NewString += "ooo"
        elif(string[i] == "O"):
           NewString += "000"
        elif(string[i] == "u" or string[i] == "U"):
            NewString += "|_|"
        else:
            NewString += string[i]
    return NewString
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console